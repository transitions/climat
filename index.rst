.. index::
   ! Transition climatique

.. raw:: html

    <a rel="me" href="https://qoto.org/@pvergain"></a>
    <a rel="me" href="https://framapiag.org/@pvergain"></a>
    <a rel="me" href="https://qoto.org/@climatejustice"></a>

.. figure:: images/climate-change_2.png
   :align: center

.. _transition_climatique:

==============================
**Transition climatique**
==============================

- https://fr.wikipedia.org/wiki/R%C3%A9chauffement_climatique

.. toctree::
   :maxdepth: 3

   humour/humour

.. toctree::
   :maxdepth: 5

   news/news
