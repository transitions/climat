.. index::
   pair: Paloma Moritz; comment agir ?
   ! Comment agir ?

===========================================================================================================
Face à l'urgence écologique et à l'inaction des dirigeants, comment agir ? #Écologie #Climat #Urgence
===========================================================================================================

- https://www.youtube.com/watch?v=8BXyHtO2mzc
- https://twitter.com/PalomaMoritz/status/1522621395674574851?s=20&t=rV9rOEA7Iaffe1O-NBZ6hA

.. figure:: images/comment_agir.png
   :align: center

   https://www.youtube.com/watch?v=8BXyHtO2mzc


Échelle individuelle
======================

- Calculer son empreinte carbone https://nosgestesclimat.fr
- Actions simples : https://bonpote.com/10-actions-simples-pour-devenir-ecolo/
- Conseils pour agir pour le climat (mode de vie, changement de banques, associations) : https://reseauactionclimat.org/agir/
- Toutes les façons de passer à l’action https://www.racinesderesilience.org
- Livre : Petit manuel de résistance contemporaine de Cyril Dion https://www.placedeslibraires.fr/livre/9782330101442-petit-manuel-de-resistance-contemporaine-cyril-dion/

Niveau professionnel :
======================

- Conseils https://bonpote.com/ecologie-dois-je-quitter-mon-travail/
- Se faire accompagner pour changer de métier  https://shiftyourjob.org
- https://monjobdesens.com
- Organiser une fresque : https://fresqueduclimat.org/organisations/
- https://www.resistanceclimatique.org/...
- Bilan carbone : https://www.cabinet-espere.fr https://www.carbone4.com/training

Médias
=======

Documentaires à voir https://www.imagotv.fr/documentaires
Nouveau hors série de Socialter sur les utopies https://fr.ulule.com/comment-nous-pourrions-vivre/

- https://le1hebdo.fr
- https://vert.eco
- https://humeco.fr/

Nouveaux imaginaires à découvrir et relayer
==============================================

- Livres et maison d’édition: https://www.lamersalee.com/a-propos/
- Films https://www.allocine.fr/film/fichefilm_gen_cfilm=227320.html

Désobéissance civile
========================

- https://extinctionrebellion.fr
- https://anv-cop21.org
- https://scientistrebellion.com
- Livre : Comment saboter un pipeline ? de Andreas Malm

Alternatives
==============

- BD La recomposition des mondes de Alessandro Pignocchi
- Podcast sur les éco lieux https://shows.acast.com/tas-de-beaux-lieux
- https://francetierslieux.fr/les-tiers-lieux-en-france/
- Documentaire ZAD : une vie à défendre ​​https://francetierslieux.fr/les-tiers-lieux-en-france/


Entretiens Blast pour aller plus loin
========================================

- Affronter le chaos qui vient : mode d’emploi avec Arthur Keller: https://www.youtube.com/watch?v=zxYQxhgYpss&t=1629s
- Plutôt couler en beauté que flotter sans grâce avec Corinne Morel Darleux: https://www.youtube.com/watch?v=zxYQxhgYpss&t=1629s
- L’enquête glaçante sur les multinationales qui brûlent notre planète avec Mickael Correia: https://www.youtube.com/watch?v=o7QM8cSr_yg&t=1124s
- Le changement peut se faire au niveau local avec Kevin Vacher https://www.youtube.com/watch?v=MxqCRVCZcI8&t=4s
- Projets polluants : guide pour résister face aux multinationales et aux Etats avec François Verdet: https://www.youtube.com/watch?v=oKQ-qsLLgV0


Journaliste : Paloma Moritz
Montage : Estelle Fromentin
Images : Arthur Frainet
Son : Marius Pruvot
Graphisme : Adrien Colrat
Diffusion : Maxime Hector
Production : Thomas Bornot
Direction des programmes : Mathias Enthoven
Rédaction en chef : Soumaya Benaissa

Le site : https://www.blast-info.fr/
Facebook : https://www.facebook.com/blastofficiel
Twitter : https://twitter.com/blast_france
Instagram : https://www.instagram.com/blastofficiel/
